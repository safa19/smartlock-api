const UserModel = require("../models/userModel");
const WithAuthKoaLambda = require("../../auth/withAuthKoaLambda");
const ConnectMongoose = require("connect-mongoose-lambda");
const validatePassword = require("../../validators/validatePassword")
const changePassword = async (user, reqBody) => {
    if (!reqBody.oldPassword || typeof reqBody.oldPassword !== "string") {
        const err = new Error("Field: \"oldPassword\" of type string is required.")
        err.status = 400;
        throw err;
    }
    if (!reqBody.newPassword || typeof reqBody.newPassword !== "string") {
        const err = new Error("Field \"newPassword\" of type string is required.");
        err.status = 400;
        throw err;
    }
    if (!validatePassword(reqBody.newPassword)) {
        const err = new Error("Invalid field 'newPassword', required min length of 4 characters.");
        err.status = 400;
        throw err;
    }
    ConnectMongoose(undefined, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useFindAndModify: false,
        useUnifiedTopology: true,
        autoIndex: true
    });

    const userDoc = await UserModel.findOneAndUpdate({
        email: user.email,
        password: reqBody.oldPassword
    }, { password: reqBody.newPassword }, { new: true });
    if (!userDoc) {
        const err = new Error("Incorrect password!");
        err.status = 400;
        throw err;
    }

    return userDoc.toObject();
}

module.exports = WithAuthKoaLambda(app => {
    app.use(async ctx => {
        const result = await changePassword(ctx.request.user, ctx.request.body);
        ctx.status = 200;
        ctx.body = result;
    });
})