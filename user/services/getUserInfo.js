const WithAuthKoaLambda = require("../../auth/withAuthKoaLambda");

module.exports = WithAuthKoaLambda(app => {
    app.use(async ctx => {
        ctx.status = 200;
        ctx.body = ctx.request.user;
    });
})