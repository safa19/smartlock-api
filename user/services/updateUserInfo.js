const UserModel = require("../models/userModel");
const WithAuthKoaLambda = require("../../auth/withAuthKoaLambda");
const ConnectMongoose = require("connect-mongoose-lambda");


const updateUser = async (user, inputModel) => {
    const userModel = new UserModel();
    userModel.email = user.email;
    userModel.password = user.password;
    userModel.firstName = inputModel.firstName || undefined;
    userModel.lastName = inputModel.lastName || undefined;
    userModel.mobilePhone = inputModel.mobilePhone || undefined;
    try {
        await userModel.validate();
    } catch (err) {
        err.status = 400;
        throw err;
    }

    ConnectMongoose(undefined, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useFindAndModify: false,
        useUnifiedTopology: true,
        autoIndex: true
    });
    const updateObj = {};
    if (userModel.firstName)
        updateObj.firstName = userModel.firstName;
    if (userModel.lastName)
        updateObj.lastName = userModel.lastName;
    if (userModel.mobilePhone)
        updateObj.mobilePhone = userModel.mobilePhone;

    try {
        return await UserModel.findByIdAndUpdate(user.id, updateObj, {
            new: true
        });
    } catch (err) {
        if (err.code === 11000) {
            err.status = 400;
            err.message = "Email is alredy in use";
            err.data = {
                email: user.email
            }
        }
        throw err;
    }
}

module.exports = WithAuthKoaLambda(app => {
    app.use(async ctx => {
        const result = await updateUser(ctx.request.user, ctx.request.body);
        ctx.status = 200;
        ctx.body = result;
    });
});